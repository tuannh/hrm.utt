﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HRM
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrConfig",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrConfig", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrCredential",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrCredential", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrManage",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrManage", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrInfo",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrInfo", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrPrint",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrPrint", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HrStatistic",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "HrStatistic", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            );
        }


    }
}
