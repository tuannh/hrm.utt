﻿$(document).ready(function () {
    $('#btn-add').on('click', function () {
        var data = $("#tbl tr:eq(1)").clone(true).appendTo("#tbl");
        data.find("input").val('');
    });
    $(document).on('click', '#btn-remove', function () {
        var trIndex = $(this).closest("tr").index();
        if (trIndex > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("Không thể xóa dòng đầu tiên");
        }
    });
});

function toggle(source, id) {
    var checkboxes = document.getElementsByName(id);
    if (source.checked) {
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = true;
        }
    } else {
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = false;
        }
    }
}
