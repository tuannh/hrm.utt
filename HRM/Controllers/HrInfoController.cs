﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRM.Controllers
{
    public class HrInfoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SoYeuLyLich()
        {
            return View();
        }

        public ActionResult DangDoanQuanNgu()
        {
            return View();
        }

        public ActionResult QuanHeGiaDinh()
        {
            return View();
        }

        public ActionResult HopDongLaoDong()
        {
            return View();
        }

        public ActionResult QuaTrinhCongTac()
        {
            return View();
        }

        public ActionResult QuaTrinhDaoTaoDaiHan()
        {
            return View();
        }

        public ActionResult TrinhDoChinhTri()
        {
            return View();
        }

        public ActionResult KhenThuong()
        {
            return View();
        }

        public ActionResult HocHam()
        {
            return View();
        }

        public ActionResult ThiDua()
        {
            return View();
        }

        public ActionResult LichSuLuong()
        {
            return View();
        }

        public ActionResult DiNuocNgoai()
        {
            return View();
        }

        public ActionResult HdLuanVan()
        {
            return View();
        }

        public ActionResult DeTaiKhoaHoc()
        {
            return View();
        }

        public ActionResult BaoCaoKhoaHoc()
        {
            return View();
        }

        public ActionResult HdXuatBan()
        {
            return View();
        }

        public ActionResult QuaTrinhGiangDay()
        {
            return View();
        }

        public ActionResult BaiBaoKhoaHoc()
        {
            return View();
        }

        public ActionResult DanhHieu()
        {
            return View();
        }

        public ActionResult KyLuat()
        {
            return View();
        }

        public ActionResult TrinhDoTinHoc()
        {
            return View();
        }

        public ActionResult TrinhDoNgoaiNgu()
        {
            return View();
        }

        public ActionResult QuaTrinhHdDang()
        {
            return View();
        }

        public ActionResult QuaTrinhHdDoan()
        {
            return View();
        }

        public ActionResult QuaTrinhHdCongDoan()
        {
            return View();
        }
    }
}
